# sync

The `sync` image provides a Docker container running [SyncThing](https://syncthing.net/).

## Usage

In order to run a basic container capable of shares volumes from another container,
start a container as follows:

    docker run 	--name some-sync --volumes-from data-container \
		-v /path-sync-config:/config \
		-p 22000:22000 \
		-d near/sync

IMPORTANT: TCP port 22000 must be open in one of nodes.


Getting Started
---------------

Take a look at the [getting started
guide](https://github.com/syncthing/syncthing/wiki/Getting-Started).

There are a few examples for keeping syncthing running in the background
on your system in [the etc directory](https://github.com/syncthing/syncthing/blob/master/etc).

There is an IRC channel, `#syncthing` on Freenode, for talking directly
to developers and users.

